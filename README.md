# README #

Api Desenvolvida utilizando Laravel 5.7.28 e fazendo uso do banco de dados MariaDB.

### Especificações Informadas ###

A API Restful deve contemplar os módulos Cliente, Pastel e Pedido, sendo que cada um devera conter endpoints CRUDL.

As tabelas devem conter as seguintes informações:

Cliente nome, email, telefone, data de nascimento, endereço, complemento, bairro, cep, data de cadastro;
Pastel nome, preço, foto;
Pedido código do cliente, código(s) do pastel, data da criação;


### Instruções de instalação ###

* Baixe sua cópia do bitbucket
* Rode o comando composer update
* Crie um banco de dados em seu Mysql com o nome API
* Altere o arquivo .env e coloque os dados de acesso ao seu banco de dados (login/senha)
* Acesse o diretório raiz da aplicação
* Rode o comando: php artisan migrate --seed (deverá criar as tabelas e inserir as informações no banco de dados
* Rode o comando php artisan serve

### EndPoints para acesso via postman ###

// rotas para clientes
- lista os clientes cadastrados no banco (use get no postman)
http://localhost:8000/api/clientes 

- detalha os dados de um cliente específico neste caso o nr 2 use get no postman
http://localhost:8000/api/exibecliente/2

- insere novo cliente (use post no postman)
http://localhost:8000/api/novocliente 

- atualiza os dados de um cliente (utilize put no postman)
http://localhost:8000/api/atualizacliente/3

- remove o cliente da base (utilize o delete no postman) 
http://localhost:8000/api/removecliente/6 

//rotas para pasteis
- lista os pasteis cadastrados no banco (use get)
http://localhost:8000/api/pasteis 

- detalhe os dados do pastel (use get no postman) 
http://localhost:8000/api/exibepastel/1

- insere novo pastel no banco (se post no postman) 
http://localhost:8000/api/novopastel 

- atualiza os dados de um pastel (use params do postman e metodo put)
http://localhost:8000/api/atualizapastel/5 

- remove o pastel do banco de dados (use delete no postman)
http://localhost:8000/api/removepastel53 

//rotas para pedidos
- lista os pedidos no banco de dados (use get no postman)
http://localhost:8000/api/pedidos  

- detalha os dados de um pedido (use get no postman)
http://localhost:8000/api/exibepedido/1 

- insere novo pedido no banco (use post no postman e informe id_cliente, id_pastel)
http://localhost:8000/api/novopedido 

- atualiza os dados de um pedido (use put no postman e informe id_cliente, id_pastel)
http://localhost:8000/api/atualizapedido

- remove o pedido do banco de dados  (use delete no postman)
http://localhost:8000/api/removepedido 

### Observações ###

* Optei por não fazer uso do passport a fim de agilizar o processo de criação e facilitar o uso (mas o correto é usar, rs 
