<?php

use Faker\Generator as Faker;

$factory->define(app\Cliente::class, function (Faker $faker) {
    return [
        //'nome', 'email', 'telefone','nascimento','endereco','complemento','bairro','cep'
        'nome' => $faker->name,
        'email' => $faker->unique()->safemail,
        'telefone' => $faker->phoneNumber,
        'nascimento' => $faker->date,
        'endereco' => $faker->streetAddress,
        'complemento' =>  $faker->secondaryAddress,
        'bairro' => $faker->name,
        'cep' => $faker->postcode

    ];
});
