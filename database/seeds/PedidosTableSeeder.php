<?php

use Illuminate\Database\Seeder;

class PedidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pedidos')->insert([
            'id_cliente' => '1',
            'id_pastel' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pedido inserido com sucesso...');
        DB::table('pedidos')->insert([
            'id_cliente' => '1',
            'id_pastel' => '2',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pedido inserido com sucesso...');
        DB::table('pedidos')->insert([
            'id_cliente' => '1',
            'id_pastel' => '3',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pedido inserido com sucesso...');
        DB::table('pedidos')->insert([
            'id_cliente' => '2',
            'id_pastel' => '2',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pedido inserido com sucesso...');
        DB::table('pedidos')->insert([
            'id_cliente' => '3',
            'id_pastel' => '3',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pedido inserido com sucesso...');


    }
}
