<?php

use Illuminate\Database\Seeder;

class PasteisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pasteis')->insert([
            'nome' => 'Especial',
            'preco' => '12.00',
            'foto' => 'especial.jpg',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pastel inserido com sucesso...');
        DB::table('pasteis')->insert([
            'nome' => 'Carne',
            'preco' => '6.00',
            'foto' => 'carne.jpg',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pastel inserido com sucesso...');
        DB::table('pasteis')->insert([
            'nome' => 'Queijo',
            'preco' => '6.00',
            'foto' => 'queijo.jpg',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pastel inserido com sucesso...');
        DB::table('pasteis')->insert([
            'nome' => 'Pizza',
            'preco' => '6.00',
            'foto' => 'pizza.jpg',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pastel inserido com sucesso...');
        DB::table('pasteis')->insert([
            'nome' => 'Bauru',
            'preco' => '6.00',
            'foto' => 'bauru.jpg',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Pastel inserido com sucesso...');

    }
}
