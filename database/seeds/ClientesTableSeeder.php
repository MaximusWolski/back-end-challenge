<?php

use Illuminate\Database\Seeder;


class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'nome' => 'Osvaldo Wolski',
            'email' => 'osvaldo@offsysbrasil.com.br',
            'telefone' => '11 2939 1055',
            'nascimento' => '1969-08-04',
            'endereco' => 'Rua da Esperança, 1138',
            'complemento' => '',
            'bairro' => 'Vila Gustavo',
            'cep' => '02208-000',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Cliente inserido com sucesso...');
        DB::table('clientes')->insert([
            'nome' => 'Mariana Julia',
            'email' => 'marianajulia@offsysbrasil.com.br',
            'telefone' => '11 2939 1055',
            'nascimento' => '1969-08-04',
            'endereco' => 'Rua da Esperança, 1138',
            'complemento' => '',
            'bairro' => 'Vila Gustavo',
            'cep' => '02208-000',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Cliente inserido com sucesso...');
        DB::table('clientes')->insert([
            'nome' => 'Marcia Maria',
            'email' => 'marciamariaa@uol.com.br',
            'telefone' => '11 2939 1055',
            'nascimento' => '1969-08-04',
            'endereco' => 'Rua da Esperança, 1138',
            'complemento' => '',
            'bairro' => 'Vila Gustavo',
            'cep' => '02208-000',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Cliente inserido com sucesso...');        
        DB::table('clientes')->insert([
            'nome' => 'Joao Leite',
            'email' => 'joaoleite@uol.com.br',
            'telefone' => '11 2939 1055',
            'nascimento' => '1969-08-04',
            'endereco' => 'Rua da Esperança, 1138',
            'complemento' => '',
            'bairro' => 'Vila Gustavo',
            'cep' => '02208-000',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $this->command->info('Cliente inserido com sucesso...');              
    }
}
