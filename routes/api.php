<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// rotas para clientes
Route::get('clientes', 'ClientesController@index');
Route::get('exibecliente/{id}', 'ClientesController@show');
Route::post('novocliente', 'ClientesController@store');
Route::put('atualizacliente/{id}', 'ClientesController@update');
Route::delete('removecliente/{id}', 'ClientesController@destroy');

//rotas para pasteis
Route::get('pasteis', 'PasteisController@index');
Route::get('exibepastel/{id}', 'PasteisController@show');
Route::post('novopastel', 'PasteisController@store');
Route::put('atualizapastel/{id}', 'PasteisController@update');
Route::delete('removepastel/{id}', 'PasteisController@destroy');

//rotas para pedidos
Route::get('pedidos', 'PedidoController@index');
Route::get('exibepedido/{id}', 'PedidoController@show');
Route::post('novopedido', 'PedidoController@store');
Route::put('atualizapedido/{id}', 'PedidoController@update');
Route::delete('removepedido/{id}', 'PedidoController@destroy');