<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pastel extends Model
{
    protected $fillable = ['nome', 'preco', 'foto'];
}
