<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\ClientesResource;
use App\Cliente;


class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::all();
        return $clientes;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
            $clientes = new Cliente;
            $clientes->nome = $request->input('nome');
            $clientes->email = $request->input('email');
            $clientes->telefone = $request->input('telefone');
            $clientes->nascimento = $request->input('nascimento');
            $clientes->endereco = $request->input('endereco');
            $clientes->complemento = $request->input('complemento');
            $clientes->bairro = $request->input('bairro');
            $clientes->cep = $request->input('cep');
            $clientes->save();
            return new ClientesResource($clientes);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id); //id comes from route
        if( $cliente ){
            return new ClientesResource($cliente);
        }
        return  response()->json("Cliente não localizado"); // temporary error
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clientes= Cliente::find($id);
        
        
        $clientes->nome  = $request->input('nome');
        $clientes->email = $request->input('email');
        $clientes->telefone = $request->input('telefone');
        $clientes->nascimento = $request->input('nascimento');
        $clientes->endereco = $request->input('endereco');
        $clientes->complemento = $request->input('complemento');
        $clientes->bairro = $request->input('bairro');
        $clientes->cep = $request->input('cep');
        $clientes->save();
        return  response()->json("Atualizado com sucesso!");
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        
        $cliente = Cliente::findOrfail($id);
        
        if($cliente->delete()){
            return  response()->json("Removido com sucesso");
        }
        return response()->json("Falha ao remover");
        
    
    }
}
