<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class PedidoController extends Controller
{
    public function index()
    {
        $pedidos = DB::table('pedidos')
        ->leftJoin('clientes', 'id_cliente', '=', 'clientes.id')
        ->leftJoin('pasteis','id_pastel', '=', 'pasteis.id')
        ->select('clientes.id','clientes.nome as cliente','pasteis.nome as pastel','pasteis.preco','pedidos.created_at')
        ->get();
        return response()->json($pedidos);

    }
    public function show($id)
    {
    $pedido = DB::table('pedidos')
    ->leftJoin('clientes', 'id_cliente', '=', 'clientes.id')
    ->leftJoin('pasteis','id_pastel', '=', 'pasteis.id')
    ->select('clientes.id','clientes.nome as cliente','pasteis.nome as pastel','pasteis.preco','pedidos.created_at')->where('pedidos.id', $id)->get();
    if( $pedido ){
        return response()->json($pedido);
    }
    return response()->json("Não localizado");
    }
    public function store(Request $request)
    {
        
        DB::table('pedidos')->insert([
            ['id_cliente' => $request->id_cliente, 'id_pastel' => $request->id_pastel,'created_at' => date("Y-m-d H:i:s"),'updated_at' => date("Y-m-d H:i:s")]
            
        ]);
        return response()->json("Inserido com sucesso!");
    }
    public function update(Request $request, $id)
{
  
    DB::table('pedidos')
    ->where('id', $id)
    ->update(['id_cliente' => $request->id_cliente, 'id_pastel' => $request->id_pastel,'updated_at' => date("Y-m-d H:i:s")]);   
   
    return response()->json("Atualizado com sucesso!");
}    
public function destroy($id)
{
    $pedido = DB::table('pedidos')->where('id', $id)->get();
      
    if($pedido){
        $pedido = DB::table('pedidos')->where('id', $id)->delete();
        return response()->json("Removido com sucesso!");
    }
    return response()->json("Não localizado!");
    }
}
