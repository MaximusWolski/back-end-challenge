<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pastel;

class PasteisController extends Controller
{
    public function index()
    {
        $pasteis = DB::table('pasteis')->get();

        return $pasteis;
    }
    public function store(Request $request)
    {
        
        DB::table('pasteis')->insert([
            ['nome' => $request->nome, 'preco' => $request->preco,'foto' => $request->foto,'created_at' => date("Y-m-d H:i:s"),'updated_at' => date("Y-m-d H:i:s")]
            
        ]);
        return response()->json("Inserido com sucesso!");

    }    

public function show($id)
{
    $pastel = DB::table('pasteis')->where('id', $id)->get();
    if( $pastel ){
        return $pastel;
    }
    return response()->json("Não localizado");
}
public function update(Request $request, $id)
{
 
    DB::table('pasteis')
    ->where('id', $id)
    ->update(['nome' => $request->nome, 'preco' => $request->preco,'foto' => $request->foto,'updated_at' => date("Y-m-d H:i:s")]);   
   
    return response()->json("Atualizado com sucesso!");
    
}
public function destroy($id)
{
    $pastel = DB::table('pasteis')->where('id', $id)->get();
    
  
    if($pastel){
        $pastel = DB::table('pasteis')->where('id', $id)->delete();
        return response()->json("Removido com sucesso!");
    }
    return response()->json("Não localizado!");
    
}
}